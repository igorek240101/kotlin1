// 1. Check out the function syntax and change the code to make the function start return the string "OK".
fun start(): String
{
    return "OK";
}
// 2. Make the function joinOptions() return the list in a JSON format (for example, [a, b, c]) by specifying only two arguments.
fun joinOptions(options: Collection<String>) =
        options.joinToString(prefix = "[", postfix = "]")
// 3. Imagine you have several overloads of 'foo()' in Java:
fun foo(name: String, number: Int = 42, toUpperCase: Boolean = false) =
        (if (toUpperCase) name.uppercase() else name) + number

fun useFoo() = listOf(
        foo("a"),
        foo("b", number = 1),
        foo("c", toUpperCase = true),
        foo(name = "d", number = 2, toUpperCase = true)
)
// 4. Replace the trimIndent call with the trimMargin call taking # as the prefix value so that the resulting string doesn't contain the prefix character.
const val question = "life, the universe, and everything"
const val answer = 42

val tripleQuotedString = """
    #question = "$question"
    #answer = $answer""".trimMargin("#")

fun main() {
    println(tripleQuotedString)
}
// 5. Using the month variable, rewrite this pattern in such a way that it matches the date in the format 13 JUN 1992 (two digits, one whitespace, a month abbreviation, one whitespace, four digits).
val month = "(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)"

fun getPattern(): String = """\d{2} ${month} \d{4}"""
// 6. Learn about null safety and safe calls in Kotlin and rewrite the following Java code so that it only has one if expression:
fun sendMessageToClient(
        client: Client?, message: String?, mailer: Mailer
) {
    val personalInfo : PersonalInfo? = client?.personalInfo;
    val email : String? = personalInfo?.email;
    if (email != null && message != null)
    {
        mailer.sendMessage(email!!, message!!);
	}
}

class Client(val personalInfo: PersonalInfo?)
class PersonalInfo(val email: String?)
interface Mailer {
    fun sendMessage(email: String, message: String)
}
// 7. Specify Nothing return type for the failWithWrongAge function. Note that without the Nothing type, the checkAge function doesn't compile because the compiler assumes the age can be null.
import java.lang.IllegalArgumentException

fun failWithWrongAge(age: Int?) : Nothing {
    throw IllegalArgumentException("Wrong age: $age")
}

fun checkAge(age: Int?) {
    if (age == null || age !in 0..150) failWithWrongAge(age)
    println("Congrats! Next year you'll be ${age + 1}.")
}

fun main() {
    checkAge(10)
}
// 8. Pass a lambda to the any function to check if the collection contains an even number. The any function gets a predicate as an argument and returns true if at least one element satisfies the predicate.
fun containsEven(collection: Collection<Int>): Boolean =
        collection.any { a -> a % 2 == 0 }